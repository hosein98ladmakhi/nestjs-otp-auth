export type CredentialsResponse = {
  accessToken: string;
  refreshToken: string;
};
