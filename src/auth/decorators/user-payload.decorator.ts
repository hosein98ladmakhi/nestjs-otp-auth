import { createParamDecorator, ExecutionContext } from '@nestjs/common';
import { Request } from 'express';

export const UserPayload = createParamDecorator(
  (data: undefined, context: ExecutionContext): Express.User => {
    return (context.switchToHttp().getRequest() as Request).user;
  },
);
