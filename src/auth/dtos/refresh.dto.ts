import { IsMongoId, IsNotEmpty, IsString } from 'class-validator';

export class RefreshDTO {
  @IsNotEmpty()
  @IsMongoId()
  userId: string;
  @IsNotEmpty()
  @IsString()
  refreshToken: string;
}
