import { Injectable } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { UsersService } from 'src/users/users.service';
import { CredentialsResponse } from './types';
import * as bcrypt from 'bcrypt';
import { RefreshDTO } from './dtos';
import { OtpService } from 'src/otp/otp.service';
import { SendOTPCodeResponse, VerifyOTPCodeResponse } from 'src/otp/types';
import { CreateOTPCodeDTO, VerifyOTPCodeDTO } from 'src/otp/dtos';

@Injectable()
export class AuthService {
  constructor(
    private readonly usersService: UsersService,
    private readonly jwtService: JwtService,
    private readonly otpService: OtpService,
  ) {}

  async sendCode(context: CreateOTPCodeDTO): Promise<SendOTPCodeResponse> {
    return await this.otpService.createOTPCode(context);
  }

  async verifyCode(
    context: VerifyOTPCodeDTO,
  ): Promise<VerifyOTPCodeResponse | CredentialsResponse> {
    const responseVerify = await this.otpService.verifyOTPCode(context);
    if (responseVerify.status === 'OK') {
      const user = await this.usersService.create({
        phoneNumber: context.phoneNumber,
      });
      const accessToken = this._generateToken(user._id);
      const refreshToken = this._generateToken(user._id);
      this.usersService.updateRefreshToken({ userId: user._id, refreshToken });
      return {
        accessToken,
        refreshToken,
      };
    }
    return responseVerify;
  }

  async refresh(context: RefreshDTO): Promise<CredentialsResponse> {
    const user = await this.usersService.findById(context.userId);
    if (!user) return;
    const isMatchRefreshToken = await bcrypt.compare(
      context.refreshToken,
      user.refreshToken,
    );
    if (!isMatchRefreshToken) return;
    const accessToken = this._generateToken(user._id);
    return {
      accessToken,
      refreshToken: context.refreshToken,
    };
  }

  private _generateToken(userId: string) {
    return this.jwtService.sign({ userId });
  }
}
