import {
  BadRequestException,
  Body,
  Controller,
  Get,
  Post,
  UseGuards,
} from '@nestjs/common';
import { CreateOTPCodeDTO, VerifyOTPCodeDTO } from 'src/otp/dtos';
import { SendOTPCodeResponse, VerifyOTPCodeResponse } from 'src/otp/types';
import { AccessTokenGuard } from './accessToken.guard';
import { AuthService } from './auth.service';
import { UserPayload } from './decorators/user-payload.decorator';
import { RefreshTokenGuard } from './refreshToken.guard';
import { CredentialsResponse } from './types';

@Controller('auth')
export class AuthController {
  constructor(private readonly authService: AuthService) {}

  @Post('code')
  async sendCode(
    @Body() context: CreateOTPCodeDTO,
  ): Promise<SendOTPCodeResponse> {
    return await this.authService.sendCode(context);
  }

  @Post('verify')
  async verifyCode(
    @Body() context: VerifyOTPCodeDTO,
  ): Promise<CredentialsResponse> {
    const response = await this.authService.verifyCode(context);
    if ((response as VerifyOTPCodeResponse)?.status === 'NO-OK') {
      throw new BadRequestException(
        (response as VerifyOTPCodeResponse)?.message,
      );
    }
    return response as CredentialsResponse;
  }

  @UseGuards(RefreshTokenGuard)
  @Post('refresh')
  async refreshToken(
    @UserPayload() payload: { userId: string; refreshToken: string },
  ): Promise<CredentialsResponse> {
    const token = await this.authService.refresh({
      userId: payload.userId,
      refreshToken: payload.refreshToken,
    });
    if (!token) throw new BadRequestException('Please Try Again');
    return token;
  }

  @UseGuards(AccessTokenGuard)
  @Get('hoo')
  hoo(@UserPayload() payload: Express.User) {
    return 'hoo';
  }
}
