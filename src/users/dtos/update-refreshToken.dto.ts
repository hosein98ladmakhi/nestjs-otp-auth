import { IsMongoId, IsNotEmpty, IsString } from 'class-validator';

export class UpdateRefreshToken {
  @IsNotEmpty()
  @IsString()
  @IsMongoId()
  userId: string;
  @IsNotEmpty()
  @IsString()
  refreshToken: string;
}
