import { IsMobilePhone, IsNotEmpty } from 'class-validator';

export class CreateUserDTO {
  @IsNotEmpty()
  @IsMobilePhone('fa-IR')
  phoneNumber: string;
}
