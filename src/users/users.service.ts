import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { User, UserDocument } from './users.schema';
import { Model } from 'mongoose';
import { CreateUserDTO, UpdateRefreshToken } from './dtos';
import * as bcrypt from 'bcrypt';

@Injectable()
export class UsersService {
  constructor(
    @InjectModel(User.name) private readonly usersModel: Model<UserDocument>,
  ) {}

  public async create(context: CreateUserDTO): Promise<UserDocument> {
    return await this.usersModel.create({ phoneNumber: context.phoneNumber });
  }

  public async findByPhone(phoneNumber: string): Promise<UserDocument> {
    return await this.usersModel.findOne({ phoneNumber });
  }

  public async updateRefreshToken(context: UpdateRefreshToken): Promise<void> {
    const refreshToken = await bcrypt.hash(context.refreshToken, 8);
    await this.usersModel.updateOne(
      { _id: context.userId },
      {
        $set: {
          refreshToken,
        },
      },
    );
  }

  public async findById(userId: string) {
    return await this.usersModel.findById(userId);
  }
}
