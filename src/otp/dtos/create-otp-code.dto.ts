import { IsMobilePhone, IsNotEmpty, IsString } from 'class-validator';

export class CreateOTPCodeDTO {
  @IsNotEmpty()
  @IsString()
  @IsMobilePhone('fa-IR')
  phoneNumber: string;
}
