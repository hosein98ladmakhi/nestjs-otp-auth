import { IsMobilePhone, IsNotEmpty, IsString, Length } from 'class-validator';

export class VerifyOTPCodeDTO {
  @IsMobilePhone('fa-IR')
  @IsNotEmpty()
  phoneNumber: string;
  @IsNotEmpty()
  @IsString()
  @Length(6)
  code: string;
}
