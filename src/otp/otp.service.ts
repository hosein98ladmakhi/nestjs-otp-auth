import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { OTP, OTPDocument } from './otp.schema';
import { Model } from 'mongoose';
import { CreateOTPCodeDTO, VerifyOTPCodeDTO } from './dtos';
import {
  GenerateOTPCodeResponse,
  SendOTPCodeResponse,
  VerifyOTPCodeResponse,
} from './types';
import * as bcrypt from 'bcrypt';

@Injectable()
export class OtpService {
  constructor(
    @InjectModel(OTP.name) private readonly OTPModel: Model<OTPDocument>,
  ) {}

  async createOTPCode(context: CreateOTPCodeDTO): Promise<SendOTPCodeResponse> {
    const { phoneNumber } = context;
    const { code, hashCode } = await this._generateOTPCode();
    const expiresAt = this._generateExpireTime();
    await this.OTPModel.create({
      code: hashCode,
      expiresAt,
      phoneNumber,
    });
    return {
      phoneNumber,
      code,
    };
  }

  async verifyOTPCode(
    context: VerifyOTPCodeDTO,
  ): Promise<VerifyOTPCodeResponse> {
    const { phoneNumber, code } = context;
    const otpDocument = await this.OTPModel.findOne({ phoneNumber });
    if (!otpDocument) return { message: 'Not Found Phone', status: 'NO-OK' };
    const isExpireOTPCode =
      new Date(otpDocument.expiresAt).getTime() <
      new Date(Date.now()).getTime();
    if (otpDocument.remainCount === 0 || isExpireOTPCode) {
      await otpDocument.delete();
      return { message: 'The OTP Code Expired ...', status: 'NO-OK' };
    }
    const isMatchCode = await bcrypt.compare(code, otpDocument.code);
    if (!isMatchCode) {
      otpDocument.remainCount = otpDocument.remainCount - 1;
      await otpDocument.save();
      return { message: 'Wrong Otp', status: 'NO-OK' };
    }
    await otpDocument.delete();
    return { message: 'OTP IS Correct', status: 'OK' };
  }

  private _generateExpireTime(): Date {
    const today = new Date(Date.now());
    today.setMinutes(today.getMinutes() + 1);
    return today;
  }

  private async _generateOTPCode(): Promise<GenerateOTPCodeResponse> {
    const code = Math.floor(100000 + Math.random() * 900000).toString();
    const hashCode = await bcrypt.hash(code, 8);
    return { code, hashCode };
  }
}
