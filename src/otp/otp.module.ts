import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { OTP, OTPSchema } from './otp.schema';
import { OtpService } from './otp.service';

@Module({
  imports: [
    MongooseModule.forFeature([
      { name: OTP.name, schema: OTPSchema, collection: 'otps' },
    ]),
  ],
  providers: [OtpService],
  exports: [OtpService],
})
export class OtpModule {}
