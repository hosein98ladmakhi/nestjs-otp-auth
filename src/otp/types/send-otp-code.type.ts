export type SendOTPCodeResponse = {
  code: string;
  phoneNumber: string;
};
