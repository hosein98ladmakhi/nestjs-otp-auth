export type GenerateOTPCodeResponse = {
  code: string;
  hashCode: string;
};
