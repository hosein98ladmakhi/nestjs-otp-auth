export type VerifyOTPCodeResponse = {
  status: 'OK' | 'NO-OK';
  message: string;
};
