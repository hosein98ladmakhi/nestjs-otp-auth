import { Schema, SchemaFactory, Prop } from '@nestjs/mongoose';
import { Document } from 'mongoose';

export type OTPDocument = OTP & Document;

@Schema({ _id: true, id: false })
export class OTP {
  @Prop({ required: true, unique: true })
  phoneNumber: string;
  @Prop({ required: true })
  code: string;
  @Prop({ required: true })
  expiresAt: Date;
  @Prop({ required: false, default: 3 })
  remainCount: number;
}

export const OTPSchema = SchemaFactory.createForClass(OTP);
